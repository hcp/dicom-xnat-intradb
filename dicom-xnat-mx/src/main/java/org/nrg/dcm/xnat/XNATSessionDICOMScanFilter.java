/**
 * Copyright (c) 2013 Washington University
 */
package org.nrg.dcm.xnat;

import java.io.File;
import java.io.FileFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A FileFilter that permits descent only into DICOM source data directories,
 * if the data are arranged in standard XNAT session format.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class XNATSessionDICOMScanFilter implements FileFilter {
    private final Logger logger = LoggerFactory.getLogger(XNATSessionDICOMScanFilter.class);
    private final File sessionRoot;

    /**
     * Create a new DICOM source directory filter for XNAT sessions.
     * @param sessionRoot root directory for this session
     */
    public XNATSessionDICOMScanFilter(final File sessionRoot) {
        this.sessionRoot = sessionRoot;
    }

    /* (non-Javadoc)
     * @see java.io.FileFilter#accept(java.io.File)
     */
    public boolean accept(final File file) {
        if (file.isFile()) {
            return true;    // don't filter individual files
        } else if (file.isDirectory()) {
            logger.trace("testing {} against root {}", file, sessionRoot);
            final File parent = file.getParentFile();
            if (sessionRoot.equals(parent)) {
                return "SCANS".equals(file.getName());
            } else {
                final File pp = parent.getParentFile();
                if (null != pp && "SCANS".equals(pp.getName()) && sessionRoot.equals(pp.getParentFile())) {
                    return "DICOM".equals(file.getName());
                } else {
                    return true;    // scan directory or some other subdirectory
                }
            }
        } else {
            return false;            // this is something weird; skip it
        }
    }
}
