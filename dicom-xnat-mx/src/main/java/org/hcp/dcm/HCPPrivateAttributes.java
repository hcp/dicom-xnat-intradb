/**
 * Copyright (c) 2012,2014 Washington University
 */
package org.hcp.dcm;

import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.SingleValueTextAttr;
import org.nrg.attr.TransformingExtAttrDef;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.SiemensPhoenixProtocolAttributeIndex;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class HCPPrivateAttributes {
    private HCPPrivateAttributes() {}
    
    public static final DicomAttributeIndex HCP_MB_RECON_LOCATION =
            new SiemensPhoenixProtocolAttributeIndex.SWipMemBlock("HCP_MB_Recon_Location", "HCP MB Recon Location", 7);
    
    // Eddie Auerbach puts a phase encoding polarity swap bit into this field.
    public static final DicomAttributeIndex HCP_EJA_POLARITY_SWAP =
            new SiemensPhoenixProtocolAttributeIndex.SWipMemBlock("HCP_EJA_Polarity_Swap", "HCP Phase Encoding Polarity Swap", 40);
    
    public static final DicomAttributeIndex HCP_RECEIVER_COIL_ID =
            new SiemensPhoenixProtocolAttributeIndex.Aliased("HCP_rcv_coil", "HCP receiver coil ID",
                    "sCoilSelectMeas.aRxCoilSelectData[0].asList[0].sCoilElementID.tCoilID",    // 3T Skyra
                    "asCoilSelectMeas[0].asList[0].sCoilElementID.tCoilID");    // CMRR 7T Trio
    
    public static ExtAttrDef<DicomAttributeIndex> receiverCoilAttribute(final String name) {
        final SingleValueTextAttr<DicomAttributeIndex> valAttr = new SingleValueTextAttr<DicomAttributeIndex>(name, HCP_RECEIVER_COIL_ID);
        return TransformingExtAttrDef.wrapTextFunction(valAttr, new Function<String,String>() {
            public String apply(final String s) {
                return null == s ? null : s.replaceAll("(^\"+)|(\"+$)", "");  // trim leading/trailing quotes
             }
        });
    }
}
