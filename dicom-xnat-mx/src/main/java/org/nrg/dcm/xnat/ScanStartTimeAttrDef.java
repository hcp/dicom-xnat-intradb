/**
 * Copyright 2012 Washington University
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.AcquisitionDate;
import static org.nrg.dcm.Attributes.AcquisitionDateTime;
import static org.nrg.dcm.Attributes.AcquisitionTime;
import static org.nrg.dcm.Attributes.SeriesDate;
import static org.nrg.dcm.Attributes.SeriesTime;
import static org.nrg.dcm.Attributes.StudyDate;

import java.util.Map;

import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;

import com.google.common.base.Strings;


/**
 * Extracts the scan start time, which is the earliest of Series Time or Acquisition Time
 * over all instances in the series.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
@Deprecated // Don't use this for HCP; check startTime comment in ImageScanAttributes.
public class ScanStartTimeAttrDef extends AbstractDateTimeAttribute implements XnatAttrDef {
    private static java.util.Date earliest(final java.util.Date...dates){
        if (null == dates || 0 == dates.length) {
            throw new IllegalArgumentException("must provide at least one Date");
        }
        java.util.Date earliest = dates[0];
        for (int i = 1; i < dates.length; i++) {
            if (null == earliest) {
                earliest = dates[i];
            } else if (null != dates[i] && dates[i].before(earliest)) {
                earliest = dates[i];
            }
        }
        return earliest;
    }

    private java.util.Date getSeriesDate(final Map<? extends DicomAttributeIndex,? extends String> m) {
        String date = m.get(SeriesDate);
        if (Strings.isNullOrEmpty(date)) {           
            date = m.get(StudyDate);
        }

        final String time = m.get(SeriesTime);
        if (Strings.isNullOrEmpty(time)) {
            return null;
        }

        final StringBuilder sb = new StringBuilder();
        sb.append(date).append(time);
        return parseDateTime(sb.toString());
    }


    public ScanStartTimeAttrDef(final String name) {
        super(name, StudyDate, SeriesDate, SeriesTime, AcquisitionDateTime, AcquisitionDate, AcquisitionTime);
    }
    
    public ScanStartTimeAttrDef() {
        this("startTime");
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#apply(java.lang.Object)
     */
    public Iterable<ExtAttrValue> apply(final java.util.Date a) throws ExtAttrException {
        return applyTime(a);
    }
    
    /*
     * NOTE: this method is not per-object thread-safe
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#foldl(java.lang.Object, java.util.Map)
     */
    public java.util.Date foldl(final java.util.Date a, final Map<? extends DicomAttributeIndex,? extends String> m)
            throws ExtAttrException {
        final java.util.Date acquisition = AcquisitionTimeAttribute.foldAcquisitionDateTime(getName(), start(), m);
        return earliest(a, null == acquisition ? getSeriesDate(m) : acquisition);
    }
}
