/**
 * Copyright (c) 2012,2013 Washington University School of Medicine
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.SOPClassUID;
import static org.nrg.dcm.Attributes.StudyInstanceUID;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dcm4che2.data.UID;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.xdat.bean.XnatPetmrsessiondataBean;
import org.slf4j.Logger;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;

/**
 * Creates a PET/MR session if both MR and PET SOPs are present in the named study.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class XnatPetmrImagesessiondataBeanFactory implements XnatImagesessiondataBeanFactory {
    private static final Iterable<String> PET_SOPs = Arrays.asList(UID.PositronEmissionTomographyImageStorage, UID.EnhancedPETImageStorage);
    private static final Iterable<String> MR_SOPs = Arrays.asList(UID.MRImageStorage, UID.EnhancedMRImageStorage);
    private static final List<Set<String>> SOP_COMBINATIONS;
    static {
        final ImmutableList.Builder<Set<String>> builder = ImmutableList.builder();
        for (final String petSOP : PET_SOPs) {
            for (final String mrSOP : MR_SOPs) {
                builder.add(ImmutableSet.of(petSOP, mrSOP));
            }
        }
        SOP_COMBINATIONS = builder.build();
    }
    
    private final Logger logger;
    
    public XnatPetmrImagesessiondataBeanFactory(final Logger logger) {
        this.logger = logger;
    }
    
    /* (non-Javadoc)
     * @see org.nrg.dcm.xnat.XnatImagesessiondataBeanFactory#create(org.nrg.dcm.DicomMetadataStore, java.lang.String)
     */
    public XnatPetmrsessiondataBean create(final DicomMetadataStore store, final String studyInstanceUID) {
        final Map<DicomAttributeIndex,ConversionFailureException> failures = Maps.newLinkedHashMap();
        final SetMultimap<DicomAttributeIndex, String> vals;
        try {
            vals = store.getUniqueValuesGiven(ImmutableMap.of(StudyInstanceUID,studyInstanceUID),
                    Arrays.asList(SOPClassUID), failures);
        } catch (Throwable t) {
            logger.error("unable to extract SOP classes for study " + studyInstanceUID, t);
            return null;
        }
        
        for (final Map.Entry<DicomAttributeIndex,ConversionFailureException> fme : failures.entrySet()) {
            logger.error("unable to convert " + fme.getKey(), fme.getValue());
        }
        
        if (null == vals || vals.isEmpty()) {
            return null;
        }
        final Set<String> sopClassUIDs = vals.get(SOPClassUID);
        if (null == sopClassUIDs || sopClassUIDs.isEmpty()) {
            return null;
        }
        for (final Set<String> sops : SOP_COMBINATIONS) {
            if (sopClassUIDs.containsAll(sops)) {
                return new XnatPetmrsessiondataBean();
            }
        }
        return null;
    }
}
