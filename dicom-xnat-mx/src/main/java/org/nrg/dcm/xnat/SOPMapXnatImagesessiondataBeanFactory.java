/**
 * Copyright (c) 2012,2013 Washington University
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.SOPClassUID;

import java.util.Set;

import org.nrg.dcm.SOPModel;
import org.slf4j.Logger;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SOPMapXnatImagesessiondataBeanFactory extends
        AttributeMapXnatImagesessiondataBeanFactory {
    private final static Function<Set<String>,String> LEAD_SOP_EXTRACTOR = new Function<Set<String>,String>() {
        public String apply(final Set<String> sops) {
            return SOPModel.getSessionType(sops);
        }
    };
    
    /**
     * @param attribute DICOM attribute
     * @param logger
     */
    public SOPMapXnatImagesessiondataBeanFactory(final Logger logger) {
        super(SOPClassUID, SOPModel.getSOPClassToSessionTypes(), LEAD_SOP_EXTRACTOR, logger);
    }
}
