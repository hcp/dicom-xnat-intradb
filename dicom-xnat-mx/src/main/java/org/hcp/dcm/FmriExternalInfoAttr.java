/**
 * Copyright (c) 2012 Washington University
 */
package org.hcp.dcm;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.SiemensShadowHeaderAttributeIndex;
import org.nrg.dcm.SiemensShadowHeaderAttributeIndex.Type;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class FmriExternalInfoAttr extends Abstract<String[]> implements XnatAttrDef {
    private static final DicomAttributeIndex fei = new SiemensShadowHeaderAttributeIndex("FmriExternalInfo", Type.SERIES);
    private static final Pattern fieldPattern = Pattern.compile("(\\w+):(.+)");

    private static final String[] extract(final String v) {
        return null == v ? null : v.split("\\|\\|");
    }

    public FmriExternalInfoAttr() {
        super(fei.getAttributeName(null), fei);
    }

    public Iterable<ExtAttrValue> apply(final String[] a) throws ExtAttrException {
        if (null == a) {
            throw new NoUniqueValueException(getName());
        }

        final List<ExtAttrValue> vals = Lists.newArrayList();
        if (!Strings.isNullOrEmpty(a[0])) {
            vals.add(new BasicExtAttrValue("fileNameUUID", a[0]));
        }
        for (int i = 1; i < a.length; i++) {
            if (!Strings.isNullOrEmpty(a[i])) {
                final Matcher m = fieldPattern.matcher(a[i]);
                if (m.matches()) {
                    final String name = "fMRI_Version_" + m.group(1);
                    final String value = m.group(2).trim();
                    Iterables.addAll(vals, AddParam.wrap(new XnatAttrDef.Constant(name, value)).apply(null));
                }
            }
        }
        return vals;
    }

    public String[] foldl(String[] a, Map<? extends DicomAttributeIndex, ? extends String> m)
            throws ExtAttrException {
        final String v = m.get(fei);
        if (null == a) {
            return extract(v);
        } else if (null == v) {
            return a;
        } else if (Arrays.equals(a, extract(v))) {
            return a;
        } else {
            throw new NoUniqueValueException(getName(),
                    new String[]{Joiner.on("||").join(Arrays.asList(a)), v});
        }
    }

    public String[] start() { return null; }
}
