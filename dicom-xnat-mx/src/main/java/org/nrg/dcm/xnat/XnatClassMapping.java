/**
 * Copyright 2010,2013 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.xdat.bean.ClassMapping;
import org.nrg.xdat.bean.base.BaseElement;

import com.google.common.base.Function;
import com.google.common.collect.Maps;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class XnatClassMapping<T extends BaseElement> implements Function<String,Class<? extends T>> {
    private static final String XNAT_NS_PREFIX = "http://nrg.wustl.edu/xnat:";
    private static final XnatClassMapping<?> instance = new XnatClassMapping<BaseElement>();

    @SuppressWarnings("unchecked")
    public static <T extends BaseElement> XnatClassMapping<T> forBaseClass(final Class<T> clazz) {
        return (XnatClassMapping<T>)instance;
    }

    private final Map<String,Class<? extends T>> beanClassCache = Maps.newHashMap();

    private XnatClassMapping() {}

    /*
     * (non-Javadoc)
     * @see com.google.common.base.Function#apply(java.lang.Object)
     */
    public Class<? extends T> apply(final String typeName) {
        try {
            return getBeanClass(typeName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a new bean object for the provided XNAT data type
     * @param type XNAT data type name (without namespace prefix: e.g., mrScanData)
     * @return newly allocated bean object
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public T create(final String type)
            throws ClassNotFoundException,IllegalAccessException,InstantiationException {
        return getBeanClass(type).newInstance();
    }

    /**
     * Gets the bean class corresponding to the provided XNAT data type name
     * @param type XNAT data type name (without namespace prefix: e.g., mrScanData)
     * @return bean class
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
    public Class<? extends T> getBeanClass(final String type)
            throws ClassNotFoundException {
        final String fqtn = XNAT_NS_PREFIX + type;
        synchronized (beanClassCache) {
            if (beanClassCache.containsKey(type)) {
                return beanClassCache.get(type);
            } else {
                final String className = ClassMapping.GetInstance().ELEMENTS.get(fqtn);
                if (null == className) {
                    throw new ClassNotFoundException("no class for type " + type);
                }
                final Class<? extends T> clazz =
                        (Class<? extends T>)XnatClassMapping.class.getClassLoader().loadClass(className);
                beanClassCache.put(type, clazz);
                return clazz;
            }
        }
    }
}
