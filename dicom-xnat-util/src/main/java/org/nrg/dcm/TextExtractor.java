/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm;

import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.nrg.util.SortedSets;

/**
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class TextExtractor implements Extractor {
    private final int tag;
    
    public TextExtractor(final int tag) {
        this.tag = tag;
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.Extractor#extract(org.dcm4che2.data.DicomObject)
     */
    public String extract(final DicomObject o) { return o.getString(tag); }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.Extractor#getTags()
     */
    public SortedSet<Integer> getTags() {
        return SortedSets.singleton(tag);
    }
}