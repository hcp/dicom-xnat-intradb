/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import org.junit.Test;
import org.nrg.attr.AbstractExtAttrDef;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;

import com.google.common.collect.ImmutableMap;

import static org.nrg.dcm.SiemensPrivateAttributes.*;

/**
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class MREchoSpacingAttributeTest {
    private ExtAttrValue getValue(final XnatAttrDef def, Map<DicomAttributeIndex,String> m) throws ExtAttrException {
        final Iterable<ExtAttrValue> vs = AbstractExtAttrDef.foldl(def, Collections.singletonList(m));
        final Iterator<ExtAttrValue> vsi = vs.iterator();
        assertTrue(vsi.hasNext());
        final ExtAttrValue v = vsi.next();
        assertFalse(vsi.hasNext());
        return v;
    }

    @Test
    public void testConvertText() throws ExtAttrException {
        final XnatAttrDef echoSpacing = new MREchoSpacingAttribute();
        assertEquals(1.0/(106*17.153),
                Double.parseDouble(getValue(echoSpacing,
                        ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "17.152999999999999",
                                SIEMENS_ACQUISITION_MATRIX_TEXT, "106*106")).getText()),
                                1e-6);
        assertEquals(1.0/(90*19.5),
                Double.parseDouble(getValue(echoSpacing,
                        ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "19.492999999999999",
                                SIEMENS_ACQUISITION_MATRIX_TEXT, "90p*106s")).getText()),
                                1e-6);
        assertEquals(1.0/(300*20),
                Double.parseDouble(getValue(echoSpacing,
                        ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "20",
                                SIEMENS_ACQUISITION_MATRIX_TEXT, "300p*320")).getText()),
                                1e-6);
        assertEquals(1.0/(94*20),
                Double.parseDouble(getValue(echoSpacing,
                        ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "20.0",
                                SIEMENS_ACQUISITION_MATRIX_TEXT, "94*110s")).getText()),
                                1e-6);
        assertEquals(1.0,
                Double.parseDouble(getValue(echoSpacing,
                        ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "1.0",
                                SIEMENS_ACQUISITION_MATRIX_TEXT, "1*1")).getText()),
                                1e-6);

    }

    @Test(expected=ConversionFailureException.class)
    public void testConvertTextNoBPPPE() throws ExtAttrException {
        final XnatAttrDef echoSpacing = new MREchoSpacingAttribute();
        getValue(echoSpacing, ImmutableMap.of(SIEMENS_ACQUISITION_MATRIX_TEXT, "106*106"));
    }
    
    @Test(expected=ConversionFailureException.class)
    public void testConvertTextNoAcqMatrix() throws ExtAttrException {
        final XnatAttrDef echoSpacing = new MREchoSpacingAttribute();
        getValue(echoSpacing, ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "17.153"));
    }
    
    @Test(expected=ConversionFailureException.class)
    public void testConvertTextBadBPPPE() throws ExtAttrException {
        final XnatAttrDef echoSpacing = new MREchoSpacingAttribute();
        getValue(echoSpacing, ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "foo",
                SIEMENS_ACQUISITION_MATRIX_TEXT, "100*100"));
    }
    
    @Test(expected=ConversionFailureException.class)
    public void testConvertTextBadAcqMatrix() throws ExtAttrException {
        final XnatAttrDef echoSpacing = new MREchoSpacingAttribute();
        getValue(echoSpacing, ImmutableMap.of(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, "17.153",
                SIEMENS_ACQUISITION_MATRIX_TEXT, "p*s"));
    }
}
