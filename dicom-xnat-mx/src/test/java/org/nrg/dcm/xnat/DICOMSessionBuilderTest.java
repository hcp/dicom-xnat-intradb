/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.taskdefs.Delete;
import org.apache.tools.ant.types.FileSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xdat.bean.XnatImagescandataBean;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DICOMSessionBuilderTest {
    private static final File sample1dir = new File(System.getProperty("sample.data.dir"));
    private static final String session_includes = "1.MR.head_DHead.*";
    private File sessionDir = null;

    @Before
    public void setUp() throws IOException {
        if (null != sessionDir) {
            throw new IllegalStateException("scan directory already assigned");
        }
        sessionDir = File.createTempFile("org.nrg.dcm.xnat.CatalogBuilder", ".test");
        sessionDir.delete();
        sessionDir.mkdir();

        final Project project = new Project();
        project.setBaseDir(sessionDir);
        final Copy copy = new Copy();
        copy.setProject(project);
        copy.setTodir(sessionDir);

        final FileSet scan4 = new FileSet();
        scan4.setIncludes(session_includes);
        scan4.setDir(sample1dir);

        copy.addFileset(scan4);
        copy.execute();
    }

    @After
    public void tearDown() {
        if (null == System.getProperty("retain-test-files")) {
            if (null == sessionDir) {
                throw new IllegalStateException("scan directory not assigned");
            }
            final Project project = new Project();
            project.setBaseDir(sessionDir);
            final Delete delete = new Delete();
            delete.setDir(sessionDir);
            delete.setVerbose(false);
            delete.execute();
        }

        sessionDir = null;
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.DICOMSessionBuilder#call()}.
     */
    @Test
    public void testCall() throws Exception {
        final DICOMSessionBuilder builder = new DICOMSessionBuilder(sessionDir);
        final XnatImagesessiondataBean session = builder.call();
        assertTrue(session instanceof XnatMrsessiondataBean);
        final Date sessionDate = new SimpleDateFormat("MM/dd/yyyy").parse("12/14/2006");
        assertEquals(sessionDate, session.getDate());
        assertEquals("09:12:06", session.getTime());
        assertEquals("Hospital", session.getAcquisitionSite());
        assertEquals("SIEMENS", session.getScanner_manufacturer());
        assertEquals("TrioTim", session.getScanner_model());
        assertEquals("MEDPC", session.getScanner());
        final List<XnatImagescandataBean> scans = session.getScans_scan();
        assertEquals(3, scans.size());
        assertTrue(scans.get(0) instanceof XnatMrscandataBean);
        final XnatMrscandataBean scan4 = (XnatMrscandataBean)scans.get(0);
        assertEquals("4", scan4.getId());
        assertEquals("1.3.12.2.1107.5.2.32.35177.3.2006121409284535196417894.0.0.0", scan4.getUid());
        assertEquals("t1_mpr_1mm_p2_pos50", scan4.getSeriesDescription());
        assertTrue(scan4.getFile().get(0) instanceof XnatResourcecatalogBean);
        final XnatResourcecatalogBean s4_catalog = (XnatResourcecatalogBean)scan4.getFile().get(0);
        assertEquals("RAW", s4_catalog.getContent());
        assertEquals("DICOM", s4_catalog.getFormat());
        assertEquals("DICOM", s4_catalog.getLabel());
        assertEquals(Double.valueOf(3.0), Double.valueOf(scan4.getFieldstrength()));
        assertEquals(Double.valueOf(1.0), scan4.getParameters_voxelres_x());
        assertEquals(Double.valueOf(1.0), scan4.getParameters_voxelres_y());
        assertEquals(Double.valueOf(1.0), scan4.getParameters_voxelres_z());
        assertEquals("Sag", scan4.getParameters_orientation());
        assertEquals(Integer.valueOf(256), scan4.getParameters_fov_x());
        assertEquals(Integer.valueOf(256), scan4.getParameters_fov_y());
        assertEquals(Double.valueOf(2400.0), scan4.getParameters_tr());
        assertEquals(Double.valueOf(3.08), scan4.getParameters_te());
        assertEquals(Double.valueOf(1000.0), scan4.getParameters_ti());
        assertEquals(Integer.valueOf(8), scan4.getParameters_flip());
        assertEquals("ORIGINAL\\PRIMARY\\M\\ND\\NORM", scan4.getParameters_imagetype());
        assertEquals("GR\\IR", scan4.getParameters_scansequence());
        assertEquals(Integer.valueOf(176), scan4.getFrames());
        assertEquals("5", scans.get(1).getId());
        assertEquals("6", scans.get(2).getId());
        builder.close();
    }
}
